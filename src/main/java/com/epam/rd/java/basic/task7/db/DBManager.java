package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.*;


public class DBManager {
    //   public class DBManager extends PostgreSqlDao {
    //  private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true"; //*for test
    private static final String DRIVER = "org.postgresql.Driver";
    private static final String URL = "jdbc:postgresql://localhost:5432/test2db";
    private static final String LOGIN = "postgres";
    private static final String PASS = "postgres";
    private static final String INSERT2 = "INSERT INTO users VALUES (?, 'David2', 'pass')";
    private static final String DELETEUSER = "DELETE from users WHERE id = ?";
    private static final String DELETETEAM = "DELETE from teams WHERE id = ?";
    private static final String INSERTTEAM = "INSERT INTO teams VALUES (DEFAULT , ?)";
    private static final String INSERTUSERTEAM = "INSERT INTO users_teams VALUES (? , ?)";
    private static final String CLEARUSERTEAMS = "DELETE from users_teams WHERE user_id = ?";
    private static final String SELECTUSERTEAM = "SELECT * FROM users_teams WHERE user_id = ? and team_id = ?";
    private static final String SELECTTEAMFORUSERS = "SELECT * FROM users_teams WHERE team_id = ?";
    private static final String CLEARTEAM = "DELETE from users_teams WHERE team_id = ?";


    private static DBManager instance;

    //private DataSource ds;
//jdbc:postgresql://localhost:5432/testdb?user=postgres&password=postgres
    public static synchronized DBManager getInstance() throws DBException {

        if (instance == null) {
            return new DBManager();
        }
        return instance;

    }

    private DBManager() throws DBException {

    }

    /*
        protected Connection getConnection() {
            try {
                Class.forName(DRIVER);
            } catch (ClassNotFoundException e) {
                System.out.println("Error load driver;");
                e.printStackTrace();
            }
            Connection c = null;
            try {
                c = DriverManager.getConnection(URL, LOGIN, PASS);
            } catch (Exception e) {
                System.out.println("Error get connection;");
                e.printStackTrace();
            }
            return c;
        }
    */
    //connection.url="jdbc:postgresql://localhost:5432/testdb?user=postgres&password=postgres" [app.properties
    Connection connection;

    public Connection getConnection() {
        try {
            Properties properties = new Properties();
            FileInputStream fis = new FileInputStream("app.properties");
            properties.load(fis);
            String url = properties.getProperty("connection.url");
            connection = DriverManager.getConnection(url);
            //connection.setAutoCommit(false);
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public List<User> findAllUsers() throws DBException {
        try (Connection c = getConnection(); Statement st = c.createStatement();) {
            ResultSet rs = st.executeQuery("SELECT * FROM users");
            List<User> out = new ArrayList<>();
            while (rs.next()) {
                out.add(new User(
                        rs.getInt("id"),
                        rs.getString("login")));
            }
            return out;
        } catch (Exception e) {
            System.out.println("Error get all users;");
            e.printStackTrace();
            return Collections.emptyList();
        }

    }

    public boolean insertUser(User user) throws DBException {
        if (getUser(user.getLogin()) != null) {
            return false;
        } else {
            int count = 0;
            try (
                    Connection c = getConnection();
                    Statement st = c.createStatement();
            ) {

                if (getUserId(user.getId()) == null) {
                    //get user from bd

                    count = st.executeUpdate("INSERT INTO users VALUES(DEFAULT , \'" + user.getLogin() + "\')");
                    User userAfter = getUser(user.getLogin());
                    user.setId(userAfter.getId());
                } else {

                    count = st.executeUpdate("UPDATE users set login=\'" + user.getLogin() + "\' WHERE id=\'" + user.getId() + "\'");
                }


            } catch (Exception e) {
                System.out.println("Error save user;");
                e.printStackTrace();
            }
            return count > 0;
        }

    }


    public boolean deleteUsers(User... users) throws DBException {
        if (Arrays.stream(users).anyMatch(Objects::isNull))  //get user from bd
            throw new IllegalArgumentException();

        User userException = null;
        Connection con = getConnection();
        try (PreparedStatement st = con.prepareStatement(DELETEUSER)) { //QUERIES.DELETEUSERS
            con.setAutoCommit(false);
            for (User user : users) {
                userException = user;
                if (getUserTeams(user).size() > 0) deleteUsersTeams(user);
                st.setInt(1, user.getId());
                //  st.setString(2, user.getLogin());
                st.executeUpdate();
            }
            try {
                con.commit();
            } catch (SQLException ex) {
                con.rollback();
            }
            con.setAutoCommit(true);
            return true;

        } catch (Exception ex) {
            throw new DBException("login: " + userException.getLogin(), ex);
        }


    }

    public User getUser(String login) throws DBException {
        try (Connection c = getConnection(); Statement st = c.createStatement();) {

            ResultSet rs = st.executeQuery("SELECT * FROM users WHERE login=\'" + login + "\'");
            if (rs.next()) {
                return new User(
                        rs.getInt("id"),
                        rs.getString("login"));
            } else return null;
        } catch (Exception e) {
            System.out.println("Error get user;");
            e.printStackTrace();
            return null;
        }
    }

    public String getUserId(int idUser) throws DBException {
        try (Connection c = getConnection(); Statement st = c.createStatement();) {

            ResultSet rs = st.executeQuery("SELECT * FROM users WHERE id=" + idUser + "");
            if (rs.next()) {
                return String.valueOf(rs.getInt("id"));
            } else return null;
        } catch (Exception e) {
            System.out.println("Error get user;");
            e.printStackTrace();
            return null;
        }
    }


    public Team getTeam(String name) throws DBException {
        try (Connection c = getConnection(); Statement st = c.createStatement();) {

            ResultSet rs = st.executeQuery("SELECT * FROM teams WHERE name =\'" + name + "\'");
            if (rs.next()) {
                return new Team(
                        rs.getInt("id"),
                        rs.getString("name"));
            } else return null;
        } catch (Exception e) {
            System.out.println("Error get team");
            e.printStackTrace();
            return null;
        }
    }

    public List<Team> findAllTeams() throws DBException {
        try (Connection c = getConnection(); Statement st = c.createStatement();) {
            ResultSet rs = st.executeQuery("SELECT * FROM teams");
            List<Team> out = new ArrayList<>();
            while (rs.next()) {
                out.add(new Team(
                        rs.getInt("id"),
                        rs.getString("name")));
            }
            return out;
        } catch (Exception e) {
            System.out.println("Error get all team");
            e.printStackTrace();
            return Collections.emptyList();
        }

    }

    public boolean insertTeam(Team team) throws DBException {
        if (getTeam(team.getName()) != null) {
            return false;
        } else {
            int count = 0;
            try (Connection c = getConnection(); Statement st = c.createStatement();) {
                //count = st.executeUpdate("INSERT INTO teams VALUES(DEFAULT,  \'" + team.getName() + "\')");
                if (getTeamId(team) < 0) {
                    //get user from bd

                    count = st.executeUpdate("INSERT INTO teams VALUES(DEFAULT,  \'" + team.getName() + "\')");
                    Team teamAfter = getTeam(team.getName());
                    team.setId(teamAfter.getId());
                } else {

                    count = st.executeUpdate("UPDATE teams set name=\'" + team.getName() + "\' WHERE id=" + team.getId() + "");
                }
            } catch (Exception e) {
                System.out.println("Error save team");
                e.printStackTrace();
            }
            return count > 0;
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        System.out.println("setTeamsForUser call");
        if (user == null || Arrays.stream(teams).anyMatch(Objects::isNull))
            throw new IllegalArgumentException();
        User userCheck = getUser(user.getLogin());


        try (Connection con = getConnection()) { //QUERIES.INSERT2
            con.setAutoCommit(false);
            try (PreparedStatement st = con.prepareStatement("INSERT INTO users_teams VALUES (? , ?)")) {
                for (Team team : teams) {
                    if (team == null || team.getId() < 1) continue;
                    st.setInt(1, userCheck.getId());
                    st.setInt(2, team.getId());
                    st.executeUpdate();
                }
                con.commit();
            }catch (SQLException ex) {
                con.rollback();
                throw new DBException(ex.getMessage(), ex);
            } finally {
                con.setAutoCommit(true);
            }
            return true;
        } catch (SQLException ex) {
            throw new DBException(user.getLogin(), ex);
        }
    }

/*    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        System.out.println("setTeamsForUser call");
        if (user == null || Arrays.stream(teams).anyMatch(Objects::isNull))
            throw new IllegalArgumentException();
        List<Team> teamsFrom = new LinkedList<>();
        User userCheck = getUser(user.getLogin());
        Team teamCheck;
        for (Team team : teams) {
            teamCheck = getTeamIdByName(team);
            teamsFrom.add(teamCheck);
        }
        System.out.println(teamsFrom);

        Connection con = getConnection();
        try (PreparedStatement st = con.prepareStatement(INSERTUSERTEAM, Statement.RETURN_GENERATED_KEYS)) { //QUERIES.INSERT2
            con.setAutoCommit(false);
            //getConnection().setAutoCommit(false);
            for (Team team : teamsFrom) {
                if (getUsersTeams(userCheck.getId(), team.getId()) > 0) continue;
                st.setInt(1, userCheck.getId());
                st.setInt(2, team.getId());
                st.executeUpdate();
            }
            try {
                con.commit();
            } catch (SQLException ex) {
                getConnection().rollback();
                con.setAutoCommit(true);
            }
            con.setAutoCommit(true);
            return true;

        } catch (SQLException ex) {
            throw new DBException(user.getLogin(), ex);
        }

    }
    */

/*
БД содержит три таблицы:
users (id, login)
teams (id, name)
users_teams (user_id, team_id)
 */

    public List<Team> getUserTeams(User user) throws DBException {
        List<Integer> teamId = new ArrayList<>();
        User userCheck = getUser(user.getLogin());

        try (Connection c = getConnection(); Statement st = c.createStatement();) {
            ResultSet rs = st.executeQuery("SELECT team_id FROM users_teams WHERE user_id =" + userCheck.getId() );
            while (rs.next()) {
                teamId.add(
                        rs.getInt("team_id"));
            }
        } catch (Exception e) {
            System.out.println("Error get  team for user");
            e.printStackTrace();
            return Collections.emptyList();
        }
        if (teamId != null || teamId.size() > 0) return getTeamId(teamId);
        return null;
    }

    /*
    Метод DBManager#deleteTeam удаляет группу по имени.
Все дочерние записи из таблицы users_teams также должны быть удалены.
Последнее реализовать с помощью каскадного ограничения ссылочной целостности:
ON DELETE CASCADE.
     */

    public boolean deleteTeam(Team team) throws DBException {
        if (getTeamForUsers(team.getId())) deleteTeamForUsers(team);

        try (PreparedStatement st = getConnection().prepareStatement(DELETETEAM)) {
            st.setInt(1, team.getId());
            st.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        if (getTeamId(team) > 0) {
            int count = 0;
            try (Connection c = getConnection(); Statement st = c.createStatement();) {
                count = st.executeUpdate("UPDATE teams set name=\'" + team.getName() + "\' WHERE id=" + team.getId() + "");
            } catch (Exception e) {
                System.out.println("Error save update team;");
                e.printStackTrace();
            }
            return count > 0;
        }
        return false;
    }


    /*****************/
    public List<Team> getTeamId(List<Integer> listIdTeam) throws DBException {
        List<Team> out = new ArrayList<>();
        for (Integer teamId : listIdTeam) {
            try (Connection c = getConnection(); Statement st = c.createStatement();) {
                ResultSet rs = st.executeQuery("SELECT * FROM teams WHERE id =" + teamId +"");
                while (rs.next()) {
                    out.add(new Team(
                            rs.getInt("id"),
                            rs.getString("name")));
                    System.out.println("name RS getTeamId "+rs.getString("name"));
                }


            } catch (Exception e) {
                System.out.println("Error get team");
                e.printStackTrace();
                return null;
            }

        }
        return out;
    }

    public int getTeamId(Team team) throws DBException {
        int idTeam = -1;

        try (Connection c = getConnection(); Statement st = c.createStatement();) {
            ResultSet rs = st.executeQuery("SELECT * FROM teams WHERE id =" + team.getId() + "");
            while (rs.next()) {
                idTeam = rs.getInt("id");
            }
            return idTeam;
        } catch (Exception e) {
            System.out.println("Error get team");
            e.printStackTrace();
            return idTeam;
        }
    }


    public Team getTeamIdByName(Team team) throws DBException {
        int idTeam = -1;
        Team teamAfter = team;

        try (Connection c = getConnection(); Statement st = c.createStatement();) {
            ResultSet rs = st.executeQuery("SELECT * FROM teams WHERE name =\'" + team.getName() + "\'");
            while (rs.next()) {
                idTeam = rs.getInt("id");
                teamAfter.setId(idTeam);
            }
            return teamAfter;
        } catch (Exception e) {
            System.out.println("Error get team");
            e.printStackTrace();
            return null;
        }

    }


    public int getUsersTeams(int idUser, int idTeam) throws DBException {
        int s = 0;
        try (Connection c = getConnection()) {
            PreparedStatement ps = preparedStatement(c, SELECTUSERTEAM);
            ps.setInt(1, idUser);
            ps.setInt(2, idTeam);
            ResultSet rs = ps.executeQuery();
            ps.close();
            while (rs.next()) {
                s++;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return s;
    }

    public boolean getTeamForUsers(int idTeam) throws DBException {
        int s = 0;
        try (Connection c = getConnection()) {
            PreparedStatement ps = preparedStatement(c, SELECTTEAMFORUSERS);

            ps.setInt(1, idTeam);
            ResultSet rs = ps.executeQuery();
            ps.close();
            while (rs.next()) {
                s++;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return (s > 0);
    }


    public boolean deleteUsersTeams(User user) throws DBException {
        Connection con = getConnection();
        try (PreparedStatement st = con.prepareStatement(CLEARUSERTEAMS)) { //QUERIES.DELETEUSERS
            st.setInt(1, user.getId());
            st.executeUpdate();
//            try {
//                con.commit();
//            } catch (SQLException ex) {
//                con.rollback();
//            }
            return true;
        } catch (Exception ex) {
            throw new DBException("error delete team login: " + user.getLogin(), ex);
        }
    }


    public boolean deleteTeamForUsers(Team team) throws DBException {

        try (PreparedStatement st = getConnection().prepareStatement(CLEARTEAM)) { //QUERIES.DELETEUSERS
            st.setInt(1, team.getId());
            st.executeUpdate();
            //   getConnection().commit();
        } catch (Exception ex) {
            throw new DBException("error delete team_users team_id: " + team.getId(), ex);
        }
        return true;
    }

    private static PreparedStatement preparedStatement(Connection c, String sql) throws SQLException {
        return c.prepareStatement(sql);

    }

}
